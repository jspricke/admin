#!/bin/sh -e
#
# https://docs.gitlab.com/ee/api/projects.html#edit-project

SERVER=gitlab.com

if [ -z "$PERSONAL_ACCESS_TOKEN" ]; then
    echo "ERROR: GitLab Token not found in PERSONAL_ACCESS_TOKEN!"
    exit 1
fi

if [ -n "$1" ]; then
    export CI_PROJECT_PATH="$1"
fi
if [ -z "$CI_PROJECT_PATH" ]; then
    echo "ERROR: project path not found in first arg or CI_PROJECT_PATH!"
    exit 1
fi
CI_PROJECT_PATH=$(echo $CI_PROJECT_PATH | sed 's,/,%2F,g')

#set -x
PROJECT_ID=$(curl --silent -f -XGET --header "PRIVATE-TOKEN: $PERSONAL_ACCESS_TOKEN" "https://${SERVER}/api/v4/projects/$CI_PROJECT_PATH" | jq '.id')

echo $PROJECT_ID
curl --silent -XPUT  --header "PRIVATE-TOKEN: $PERSONAL_ACCESS_TOKEN" \
     "https://${SERVER}/api/v4/projects/$PROJECT_ID/\
?analytics_access_level=disabled\
&autoclose_referenced_issues=true\
&container_registry_enabled=false\
&lfs_enabled=false\
&operations_access_level=disabled\
&packages_enabled=false\
&request_access_enabled=false\
&requirements_enabled=false\
&security_and_compliance_enabled=false\
&service_desk_enabled=false\
&snippets_access_level=disabled\
&wiki_access_level=disabled" \
     | jq --sort-keys .
